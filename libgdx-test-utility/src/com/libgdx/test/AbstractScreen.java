package com.libgdx.test;


import com.badlogic.gdx.Screen;
import com.libgdx.test.Basic3DTest;


public abstract class AbstractScreen implements Screen {

   protected Basic3DTest game;

   public AbstractScreen(Basic3DTest game) {
       this.game = game;
   }

   @Override
   public void pause() {
   }

   @Override
   public void resume() {
   }

   @Override
   public void dispose() {
   }
}
