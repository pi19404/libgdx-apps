package com.libgdx.test;


import com.badlogic.gdx.utils.Disposable;

public abstract class GameCharacter implements Disposable
{
	public abstract  void create();
	public abstract void render();
	public abstract void dispose();
	public abstract void onLoaded();
	public abstract void loadAssets();
}
