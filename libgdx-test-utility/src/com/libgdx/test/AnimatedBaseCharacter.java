package com.libgdx.test;




import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.model.Animation;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController.AnimationDesc;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController.AnimationListener;
import com.badlogic.gdx.math.Vector3;



public class AnimatedBaseCharacter extends BaseCharacter implements AnimationListener {

	Vector3 velocity;
	
	AnimationController animation;
	final AnimationController.Transform trTmp = new AnimationController.Transform();
	final AnimationController.Transform trForward = new AnimationController.Transform();
	final AnimationController.Transform trBackward = new AnimationController.Transform();
	final AnimationController.Transform trRight = new AnimationController.Transform();
	final AnimationController.Transform trLeft = new AnimationController.Transform();

	public AnimatedBaseCharacter(String name,int mass,String asset,AssetManager assets,Pose p1)
	{
		super(name,mass,asset,assets, p1);

	}
	
	public AnimatedBaseCharacter(String name,int mass,String asset,AssetManager assets,Pose p1,GameWorld w)
	{
		super(name,mass,asset,assets, p1);

	}
	public AnimatedBaseCharacter()
	{
		super();

	}
	@Override
	public void dispose () {
		super.dispose();
	}
	
	
	
	@Override
	public void onEnd(AnimationDesc animation1) {
		// TODO Auto-generated method stub
		
		Gdx.app.log("AAAA ",animations.get(current));
		
		current=current+1;
		if(current>=animations.size())
			current=0;
		flag=false;
		//animation.setAnimation(null);
		//animation.setAnimation(animations.get(current),(AnimationListener)this);
		
	}

	@Override
	public void onLoop(AnimationDesc animation) {
		// TODO Auto-generated method stub
		
	}
	

	boolean flag=false;
	int current=0;
	
	@Override
	public void render()
	{
		super.render();
		
		
		if(character!=null)
		{
			
			if(animation!=null)
			{
			
			/*if(animation.current!=null)
			{
			Gdx.app.log("current",animation.current.animation.id);
			}*/
			animation.update(Gdx.graphics.getDeltaTime());
			if(flag==false)
			{
				animation.setAnimation(null);
				animation.setAnimation(animations.get(current), this);
				flag=true;
			}
			}
			
		}
	}
	
	List<String> animations = new ArrayList<String>();
	int count=0;
	@Override
	public void onLoaded()
	{
		super.onLoaded();
		if(character!=null)
		{
			animation = new AnimationController(character);
			//Gdx.app.log("Test", "number of animations loaded are "+character.animations.size);
			for (Animation anim : character.animations)
			{
				Gdx.app.log("Test", "ZZ"+anim.id);
				animations.add(anim.id);
			}
				///count=character.animations.size;
			
			//animation.animate("Default Take", -1, 1f, null, 0.2f);
			
		}
	}
}
