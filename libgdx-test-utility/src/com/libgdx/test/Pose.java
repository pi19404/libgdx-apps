package com.libgdx.test;


import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;

public class Pose
{
	public Vector3 mPosition;
	public Quaternion mOrientation;
	public Vector3 mScale;
	
	public Pose()
	{
		mPosition=new Vector3(0,0,0);
		mScale=new Vector3(1,1,1);
		mOrientation=new Quaternion(0,0,0,0);
	}
	public Pose(Vector3 v,Quaternion q,Vector3 scale)
	{
		mPosition=v.cpy();
		mOrientation=q.cpy();
		mScale=scale.cpy();
	}
}
