package com.libgdx.test;




import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.graphics.g3d.utils.DepthShaderProvider;
import com.badlogic.gdx.graphics.g3d.utils.TextureProvider;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;


import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.UBJsonReader;

public class BaseCharacter extends GameCharacter 
{
	ModelInstance character;
	boolean enable=true;
	public Matrix4 transform;	
	Model m;
	int id;
	Axes axes;
	String asset_name;
	public AssetManager assets=null;
	public boolean loading=true;
	public Pose p=new Pose();
	GameWorld w;
	void setWorld(GameWorld w)
	{
		this.w=w;
	}
	
	void setPose(Pose target)
	{
		p.mPosition=target.mPosition.cpy();
		p.mOrientation=target.mOrientation.cpy();
			
	}
	
	Pose getPose()
	{
		return p;
	}
	public Array<ModelInstance> instances = new Array<ModelInstance>();

	public String name=new String();
	
	
	
	
	public final ModelInstance getActor()
	{
		return character;
	}
	
	public BaseCharacter(ModelInstance m)
	{
	
		character=new ModelInstance(m);

		loading=false;
	}
	
	public BaseCharacter()
	{
		
	}
	
	public BaseCharacter(String name,int id,String asset,AssetManager assets,Pose p1)
	{
		this.id=id;
		this.name=name;
		asset_name=asset;
		this.assets=assets;

		create();

		p.mOrientation=p1.mOrientation.cpy();
		p.mPosition=new Vector3(p1.mPosition.x,p1.mPosition.y,p1.mPosition.z);
		p.mScale=new Vector3(p1.mScale.x,p1.mScale.y,p1.mScale.z);
	}
	
	public BaseCharacter(String asset,AssetManager assets,Pose p1)
	{
		asset_name=asset;
		this.assets=assets;

		create();

		p.mOrientation=p1.mOrientation.cpy();
		p.mPosition=new Vector3(p1.mPosition.x,p1.mPosition.y,p1.mPosition.z);
		p.mScale=new Vector3(p1.mScale.x,p1.mScale.y,p1.mScale.z);
		
	}
	
	@Override
	public  void create()
	{

		loadAssets();
	}
	
	synchronized void  updateModel()
	{
		if (character != null) {
			
		character.transform.idt();
		
		character.transform.translate(p.mPosition.cpy());
		character.transform.rotate(p.mOrientation.cpy());		
		character.transform.scale(p.mScale.x,p.mScale.y,p.mScale.z);

		}
	}
	@Override
	public void render()
	{
		
		updateModel();
		
		if (loading &&  assets!=null && assets.update()) {		
			loading = false;
			onLoaded();
			Gdx.app.log("ZZ","copmpleted loading "+name);
		}	
	}



	@Override
	public void dispose()
	{
		character=null;
		loading=true;

	}
	
	@Override
	public void loadAssets()
	{
		assets.load(asset_name, Model.class);		
	}
	
		@Override
		public synchronized void onLoaded()
		{
			if(character==null)
			{
				
				
				
				
				//G3dModelLoader loader = new G3dModelLoader(new JsonReader());
				///m = loader.loadModel(Gdx.files.internal(asset_name));
				if(asset_name.contains("g3db"))
				m=new G3dModelLoader(new UBJsonReader()).loadModel(Gdx.files.internal(asset_name),new TextureProvider.FileTextureProvider());
				else if(asset_name.contains("g3dj"))
					m=new G3dModelLoader(new JsonReader()).loadModel(Gdx.files.internal(asset_name),new TextureProvider.FileTextureProvider());
				else
				m = assets.get(asset_name, Model.class);
				 /*
				  UBJsonReader jsonReader = new UBJsonReader();
		        // Create a model loader passing in our json reader
		        G3dModelLoader modelLoader = new G3dModelLoader(jsonReader);
		        // Now load the model by name
		        // Note, the model (g3db file ) and textures need to be added to the assets folder of the Android proj
		        m = modelLoader.loadModel(Gdx.files.getFileHandle(asset_name, FileType.Internal));
				*/
			///m=assets.get(asset_name, Model.class);
			character = new ModelInstance(m);
			BoundingBox bbox = new BoundingBox();
			character.calculateBoundingBox(bbox);			
			instances.add(character);

			}
		}

}

