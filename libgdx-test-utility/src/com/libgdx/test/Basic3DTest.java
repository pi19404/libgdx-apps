package com.libgdx.test;






import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.FloatAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalShadowLight;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.DepthShaderProvider;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;


import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;




public class Basic3DTest extends GdxTest implements GestureListener
{

	public boolean renderFlag=true;
	public GameWorld world;

	public CameraInputController inputController;
	public boolean loading=false;
	final Vector3 tmpVector = new Vector3();

	String model;
	
	@Override
	public void dispose()
	{
		world.dispose();
	}
	
	
	//configuring the input sensors
	@Override
	public void create()
	{		
		world=new GameWorld(model);
		//world.setModel(model);
		Gdx.app.log("FF1",model);
		Gdx.input.setInputProcessor(inputController = new CameraInputController(world.rotatecam));
	}
	float [] size=null;
	
	public void updateSize(float []size)
	{
		world.size=size;
	}
	
	
	@Override
	public void render()
	{
		renderFlag=true;		
		world.render(renderFlag,Gdx.graphics.getDeltaTime());
		
	}			
				
		
	//update the camera position from the market
	public void updateCamera(Vector3 camDirection,Vector3 camPosition,Vector3 camUp) {

		if(update_camera_flag)
		world.updateCamera(camDirection, camPosition, camUp);

	}
	
	public Basic3DTest(String name)
	{
		model=name;
	}

	

	@Override
	public void resize (int width, int height) {
		super.resize(width, height);
		/*if(menu!=null)
		{
		menu.hud.getViewport().update(width, height, true);
		menu.hudWidth = menu.hud.getWidth();
		menu.hudHeight = menu.hud.getHeight();
		}*/
		//if(l!=null)
		//l.resize(width, height);
	}	
	

	
	
	
	public void shoot (final float x, final float y) {
		 shoot(x, y, 150f);
	}

	public void shoot (final float x, final float y, final float impulse) {
		 shoot("box", x, y, impulse);
	}

	Vector3 old=new Vector3();
	public void shoot (final String what, final float x, final float y, final float impulse) {
		// Shoot a box
		
		Vector3 rayFrom,rayTo;
		rayTo=new Vector3(0,0,0);
		rayFrom=new Vector3(0,0,0);
		Ray ray = world.rotatecam.getPickRay(x, y).cpy();
		
		
		

	}
	
 	@Override
		public boolean tap (float x, float y, int count, int button) {
 		
 		Gdx.app.log("BB","completed  tap");
 	
			return true;
		}
 	
	@Override
	public boolean touchDown (float x, float y, int pointer, int button) {
		
	
		return false;
	}
	
	
	boolean update_camera_flag=true;
	boolean state=false;
	@Override
	public boolean longPress (float x, float y) {

		/*Gdx.app.log("ZZ","Dragged "+x+":"+y+":"+Gdx.graphics.getWidth()+":"+Gdx.graphics.getHeight()+":"+world.aflag);
		
		if(state==false)
		{
			world.aflag=1;
			state=true;
		}
		else
		{
			world.resetWorld();
			world.aflag=0;
			state=false;
		}
		

		return false;
		*/
		return false;
	}
	@Override
	public boolean fling (float velocityX, float velocityY, int button) {
	//	Log.e("BB","completed  panning");
	//	update_camera_flag=true;
		return false;
	}
	@Override
	public boolean pan (float x, float y, float deltaX, float deltaY) {
		//Log.e("BB","panning start");
		//update_camera_flag=false;
		return false;
	}
	@Override
	public boolean panStop (float x, float y, int pointer, int button) {
		//Log.e("BB","completed  panning");
		//update_camera_flag=true;
		return false;
	}
	@Override
	public boolean zoom (float initialDistance, float distance) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean pinch (Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		// TODO Auto-generated method stub
		return false;
	}
	
}

