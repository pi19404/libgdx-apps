package com.libgdx.test;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;



import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalShadowLight;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController.AnimationDesc;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.DepthShaderProvider;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController.AnimationListener;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;


import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.UBJsonReader;
import com.badlogic.gdx.utils.viewport.ScreenViewport;



public class GameWorld <T extends BaseCharacter> {
		
	//game environments
	public PerspectiveCamera cam;
	public PerspectiveCamera rotatecam;
	public float []size=null;
	Environment lights;
	DirectionalShadowLight shadowLight;
	public ModelBatch batch;	
	ModelBatch shadowBatch;

	//game assets
	public AssetManager assets=null;
	
	//game elements
	
	public AnimatedBaseCharacter hero1;

	
	//element id and index map
	public Map<Integer, Integer> myMap = new HashMap<Integer, Integer>();
	
	//array of characters for rendering etc
	public final Array<T> entities = new Array<T>();
		
	
	//update the camera from AR
	public void updateCamera(Vector3 camDirection,Vector3 camPosition,Vector3 camUp) {

		if(cam!=null)
		{
	

		cam.direction.set(camDirection);
		cam.up.set(camUp);
		cam.position.set(camPosition);


		cam.update();
		
		}	
}
	int count=0;
	
	//return the camera
	public Camera getCamera()
	{
		return cam;
	}
	String model;

	void setModel(String name)
	{
		model=name;		
	}
	
	public GameWorld()
	{
		super();
		create();
		initializeActors();


	}

	
	public GameWorld(String model)
	{
		super();
		setModel(model);
		create();
		initializeActors();


	}

	
	public GameWorld(boolean flag)
	{
		if(flag==true)
		create();
	}
	
	//first step of rendering
	protected void beginRender (boolean lighting) {

		Gdx.gl.glClearColor(0,0,0,0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

	}
	
	//render function called every frame
	public void render(boolean flag,float deltaTime)
	{
		
			beginRender(true);
			if(flag==true)
			renderWorld(deltaTime);
			

	}
	

	public void render (final ModelBatch batch, final Environment lights) {
		
		render(batch, lights,entities);
	}
	
	public  T add (final T entity) {
				
		entity.id=count;
		myMap.put(entity.id,entities.size);
		entities.add(entity);
	
		count=count+1;
		return entity;
		
	}
	
	//render game characters
	public void render (final ModelBatch batch, final Environment lights, final Iterable<T> entities) {
		for (final T e : entities) {

			e.render();
			//if(e.id==1)
			//Log.e("ZZ","e.postion"+e.p.mPosition.x+":"+e.enable);
			if (e.instances != null) 
			{
				if(e.enable==true)
				batch.render(e.instances,lights);
	
			}

		}
	
		
	}
	
	
	public int getElementIndex(int id)
	{
		return (Integer)myMap.get(id);
	}
	
	public synchronized void deleteElement(int id)
	{
		int ch=getElementIndex(id);
		T e=entities.removeIndex(ch);
		
		
		myMap.remove(id);
		
		
		//update array indexes
		updateIndices(ch);
	}
	
	public void updateIndices(int k)
	{
		for(int i=0;i<entities.size;i++)
		{
			BaseCharacter c1=(BaseCharacter)getEntity(i);
			if(i>=k)
			{
				myMap.put(c1.id,i);
			}
		}
	}
	
	//get entoty from id
	public T getEntityId(int id)
	{
		int ch=getElementIndex(id);
		return getEntity(ch);
	}
	
	//get entoty based on index
	public T getEntity(int index)
	{
		return entities.get(index);
	}
	
	
	float prev_yaw=-999;
	float angle=0;
	
	
	//update the character positions for rendering
	public void renderWorld(float deltaTime)
	{
		

			rotatecam.update();
			
		 	batch.begin(rotatecam);

			render(batch, lights);
		
			batch.end();
      
	}
	


	
	public void dispose()
	{
		
		shadowLight.dispose();
		assets.dispose();		

			//disposing each element and clearning the array
			for (int i = 0; i < entities.size; i++)
				entities.get(i).dispose();
			entities.clear();
				
	}
	
	public void loadAssets()
	{
		//assets.load("data/g3d/A.g3db", Model.class);
	   //assets.load("data/g3d/r3.g3db",Model.class);
		Gdx.app.log("FF",model);
		assets.load(model,Model.class);
		//assets.load("data/g3d/iPhone5.obj",Model.class);
	}
	
	
	public void initializeActors()
	{
		Pose p=new Pose();
		p.mPosition=new Vector3(0,0,0f);
		p.mOrientation.setFromAxis(new Vector3(1,0,0),90);
		
		p.mScale=new Vector3(1f,1f,1f);
		
		
		if(model.endsWith("obj"))
		{		
		BaseCharacter hero1=new BaseCharacter("hero",0,model,assets,p);
		hero1=(BaseCharacter)add((T)hero1);
		}
		else
		{
			AnimatedBaseCharacter hero1=new AnimatedBaseCharacter("hero",0,model,assets,p);
			hero1=(AnimatedBaseCharacter)add((T)hero1);			
		}
		
//		p.mPosition=new Vector3(20,0,0f);
	//	p.mScale=new Vector3(1f,1f,1f);
		
		
		
		
		
	}

	
	
	public void create() {
		
		//initialize the environment
		batch = new ModelBatch();
		shadowBatch = new ModelBatch(new DepthShaderProvider());
		lights = new Environment();
		lights.set(new ColorAttribute(ColorAttribute.AmbientLight, 1f, 1f, 1f, 1.f));
		lights.add((shadowLight = new DirectionalShadowLight(1024, 1024, 30f, 30f, 1f, 100f))
.set(0.8f, 0.8f, 0.8f, -.4f, -.4f, -.4f));		
			
		
		
		
		if (assets == null) assets = new AssetManager();
		
		loadAssets();
		
		
		
		cam = new PerspectiveCamera(60, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
      
      	cam.position.set(0,35,85);
		cam.lookAt(0, 0, 0);
		
		cam.near = 0.1f;
		cam.far = 2000f;
		cam.update();
		
		rotatecam= new PerspectiveCamera(40, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		rotatecam.position.set(0, 35,85);
		rotatecam.lookAt(0, 0, 0);
		
		rotatecam.near = 0.1f;
		rotatecam.far = 2000f;
		rotatecam.update();	
	}


	}
